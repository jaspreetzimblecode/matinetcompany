import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoManagementComponent } from './video-management/video-management.component';
import { ViewVideoComponent } from './view-video/view-video.component';


const routes: Routes = [
  {path:"",component:VideoManagementComponent,
children:[
  { path: '', redirectTo: '/main/video/view-video', pathMatch: 'full' },
  {path:"view-video",component:ViewVideoComponent},
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyvideoRoutingModule { }
