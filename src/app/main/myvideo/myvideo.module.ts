import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyvideoRoutingModule } from './myvideo-routing.module';
import { VideoManagementComponent } from './video-management/video-management.component';
import { ViewVideoComponent } from './view-video/view-video.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [VideoManagementComponent, ViewVideoComponent],
  imports: [
    CommonModule,
    MyvideoRoutingModule,
    SharedModule
  ]
})
export class MyvideoModule { }
