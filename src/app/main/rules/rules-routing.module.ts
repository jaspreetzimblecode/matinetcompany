import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RulesManagementComponent } from './rules-management.component';
import { ViewRulesComponent } from './view-rules/view-rules.component';


const routes: Routes = [
  {path:"",component:RulesManagementComponent,
  children:[
  { path: '', redirectTo: '/main/rules-management/view-rules', pathMatch: 'full' },
  {path:"view-rules",component:ViewRulesComponent}
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RulesRoutingModule { }
