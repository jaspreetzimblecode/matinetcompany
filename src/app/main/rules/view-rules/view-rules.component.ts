import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { RuleService } from '../../rules/rule.service';
import { DistanceService } from '../../distance/distance.service';

@Component({
  selector: 'app-view-rules',
  templateUrl: './view-rules.component.html',
  styleUrls: ['./view-rules.component.css']
})
export class ViewRulesComponent implements OnInit {
  isOpen:boolean
  panelOpenState = false;
  name = "ng2-ckeditor";
  ckeConfig: any;
  mycontent: string;
  log: string = ""; 
  isEditStep: string = "false";
  isEditAwarness: string = "false";
  awarenessList: [];
  stepList: [];
  id: string = "";
  type: number = 1;
  ruleText: string = "";
  textTerm: string = "";
  textSupport: string = "";
  textAbout: string = "";
  contents:any;
  config;
  innerHtml;

  titleName:string;
  isadd:boolean = false;
  rowDetail:any;
  selectedfile: any;
  btntext="button"
  query: string;
  title = "";
  showimg:any;
  statusFormSubmitted: boolean = false;
  modalForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
// filters end

list = [];
ruleList = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private distanceService:DistanceService,
    private ruleService:RuleService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.ckeConfig = {
      allowedContent: false,
      extraPlugins: "divarea",
      forcePasteAsPlainText: true
    };
    // this.getCategoryList();
    this.getRuleList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)

  toggle(expanded) {
    expanded = !expanded;
  }
  
  getRuleList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.ruleService.getRuleListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.ruleList = res["data"]["rulesList"];
          this.ruleList['expanded'] = false;
          console.log(this.list)
        } else {
          this.ruleList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getRuleList();
  }

  searchFiltersBtn() {
    this.getRuleList();
  }
 
  openModal(data, content, btn,value) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'xl'
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
if(value =='add') {
  console.log("add");
  this.title = "Add Rules"
}if (value =='edit') {
this.rowDetail = data;
  this.title = "Edit Rules"
this.ruleText = data['rules']
this.titleName = data['title']
}
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.title = "";
        this.ruleText = "";
        this.titleName = "";
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  postCategory() {
      this.isLoading = true ;
      console.log();
      if(this.title == 'Add Rules') {
        this.addRule();
        } if(this.title == 'Edit Rules') {
       this.editCategory();
       
        }
        this.title = "";
        err => {
          this.isLoading = false;
          this.modalRef.close();
          this.showimg = "";
        }
  }

  deleteImg() {
    this.showimg = "";
  }

  addRule () {
    console.log("ADD")
   let finalObj = {};
   finalObj['rules'] = this.ruleText; 
   finalObj['title'] = this.titleName;
   console.log(finalObj);
    this.ruleService.postRuleListApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
  
              this._snackBar.open("Rule created successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            });
            this.titleName = "";
            this.ruleText = "";
            this.getRuleList();
            this.modalRef.close();
        } 
      })
  }

  editCategory () {
    console.log("EDIT")
    let finalObj = {};
    finalObj['rules'] = this.ruleText; 
    finalObj['title'] = this.titleName;
    finalObj['ruleId'] = this.rowDetail['ruleId'];
    this.ruleService.putRuleListApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
            this._snackBar.open("Rules updated successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            });
            this.ruleText = "";
            this.titleName = "";
            this.getRuleList();
            this.modalRef.close();
        } 
      })
  }

  deleteAction(item) {
    Swal.fire({
      title: "You want to delete: " + item.title,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.value) {
        console.log(result.value)
        this.isLoading = true;
        this.ruleService.deleteRuleListApi(item['ruleId']).subscribe(
          res => {
            this.isLoading = false;
            if (res["message"] == "success") {
              this._snackBar.open("Rules deleted successfully!","",{
                duration: 5000,
                horizontalPosition:'right',
                verticalPosition:'top',
                panelClass: ['success']
              });
            } else {
              Swal.fire("Error", res.data, "error");
            }
            this.isLoading = false;
            this.getRuleList();
          },
          err => {
            this.isLoading = false;
          }
        );
      }
    });
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
