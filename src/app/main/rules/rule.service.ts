import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class RuleService {


  constructor(private _networkService:NetworkService) { }

  getRuleListApi(query:any) {
    return this._networkService.get(
      "api/rule?" + query,
      null,
      null,
      "bearer"
    );
  }

  postRuleListApi(body:any) {
    return this._networkService.post(
      "api/rule",
      body,
      null,
      "bearer"
    );
  }
  
  putRuleListApi(body:any) {
    return this._networkService.put(
      "api/rule",
      body,
      null,
      "bearer"
    );
  }
  deleteRuleListApi(id:any) {
    return this._networkService.delete(
      "api/rule/"+id,
      null,
      null,
      "bearer"
    );
  }

}
