import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RulesRoutingModule } from './rules-routing.module';
import { RulesManagementComponent } from './rules-management.component';
import { ViewRulesComponent } from './view-rules/view-rules.component';
import { RuleService } from './rule.service';
import { SharedModule } from '../../shared/shared.module';
import { CKEditorModule } from 'ng2-ckeditor';


@NgModule({
  declarations: [RulesManagementComponent,ViewRulesComponent],
  imports: [
    CommonModule,
    RulesRoutingModule,
    SharedModule,
    CKEditorModule
  ],
  providers:[RuleService]
})
export class RulesModule { }
