import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class RaceService {
  constructor(private _networkService:NetworkService) { }

  getRaceListApi(query:any) {
    return this._networkService.get(
      "api/race/admin?" + query,
      null,
      null,
      "bearer"
    );
  }
  getParticipantListApi(raceId:any) {
    return this._networkService.get(
      "api/participant/" + raceId,
      null,
      null,
      "bearer"
    );
  }
  updateParticipantStatus(body:any) {
    return this._networkService.put(
      "api/participant",
      body,
      null,
      "bearer"
    );
  }
}
