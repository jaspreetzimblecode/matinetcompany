import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { RuleService } from '../../rules/rule.service';
import { RaceService } from '../race.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-participant',
  templateUrl: './view-participant.component.html',
  styleUrls: ['./view-participant.component.css']
})
export class ViewParticipantComponent implements OnInit {
  isadd:boolean = false;
  rowDetail:any;
  selectedfile: any;
  btntext="button"
  query: string;
  title = "";
  showimg:any;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "accepted", name: "Accept" },
    { id: "rejected", name: "Reject" },
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
// filters end

list = [];
ruleList = [];
participant=[];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  raceId:string;
  private modalRef: NgbModalRef;

  constructor(private raceService:RaceService,
    private ruleService:RuleService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,) { }

  ngOnInit() {
    this.raceId = this.route.snapshot.params["raceId"];
    
    this.getParticipent(this.raceId);
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)

  onNavigateBack() {
    this.router.navigate(["/main/race-management/view-race"]);
  }
  getParticipent(raceId) {
    this.isLoading = true;
    this.raceService.getParticipantListApi(raceId).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.participant = res["data"]["participantList"];
          console.log(this.participant,'ifal')
        } else {
          this.list = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }


  participantModal (row, content, btn) {
    this.rowDetail = [];
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();
    this.rowDetail = row;
    if(this.rowDetail.joinedParticipants == 0) {
      return
    }
    this.getParticipent(this.rowDetail['raceId'])
  
   
    this.modalRef = this.modalService.open(content, {
      ...this.modelOptions,
      size: "lg"
    });
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

  }
    

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getParticipent(this.raceId);
  }

  searchFiltersBtn() {
    this.getParticipent(this.raceId);
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      status: ["", [Validators.required]]
    });
  }
  
  updateUserStatusModal(row, content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md'
    };
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.rowDetail = row;
    this.prepareUpdateStatusForm();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateUserStatus(test) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.isLoading = true ;
      let finalObj = {
        participantId: this.rowDetail["participantId"],
        status: this.statusForm.controls.status.value
      };
      this.raceService.updateParticipantStatus(finalObj).subscribe(
        res => {
          this.isLoading = false;
          if (res["message"] == "Success") {
            this._snackBar.open("status updated successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            });
            // Swal.fire("Success", "Officer updated successfully!", "success");
            this.getParticipent(this.raceId);
            this.modalRef.close();
            this.statusForm.reset();
          
          } else {
          }
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }


}
