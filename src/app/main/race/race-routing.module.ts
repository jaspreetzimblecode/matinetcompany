import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RaceManagementComponent } from './race-management.component';
import { ViewRaceComponent } from './view-race/view-race.component';
import { ViewParticipantComponent } from './view-participant/view-participant.component';


const routes: Routes = [
  {path:"",component:RaceManagementComponent,
  children:[
  { path: '', redirectTo: '/main/race-management/view-race', pathMatch: 'full' },
  {path:"view-race",component:ViewRaceComponent},
  {path:"view-participant/:raceId",component:ViewParticipantComponent}
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RaceRoutingModule { }
