import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RaceRoutingModule } from './race-routing.module';
import { RaceManagementComponent } from './race-management.component';
import { ViewRaceComponent } from './view-race/view-race.component';
import { RaceService } from './race.service';
import { SharedModule } from '../../shared/shared.module';
import { ViewParticipantComponent } from './view-participant/view-participant.component';


@NgModule({
  declarations: [RaceManagementComponent, ViewRaceComponent, ViewParticipantComponent],
  imports: [
    CommonModule,
    RaceRoutingModule,
    SharedModule
  ],
  providers:[RaceService]
})
export class RaceModule { }
