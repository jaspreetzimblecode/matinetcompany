import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateManagementComponent } from './create-management/create-management.component';
import { CreatePlaylistComponent } from './create-playlist/create-playlist.component';


const routes: Routes = [
  {path:"",component:CreateManagementComponent,
children:[
  { path: '', redirectTo: '/main/playlist/create', pathMatch: 'full' },
  {path:"create",component:CreatePlaylistComponent},
]
}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreatePlaylistRoutingModule { }
