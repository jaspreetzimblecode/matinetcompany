import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreatePlaylistRoutingModule } from './create-playlist-routing.module';
import { CreateManagementComponent } from './create-management/create-management.component';
import { CreatePlaylistComponent } from './create-playlist/create-playlist.component';


@NgModule({
  declarations: [CreateManagementComponent, CreatePlaylistComponent],
  imports: [
    CommonModule,
    CreatePlaylistRoutingModule
  ]
})
export class CreatePlaylistModule { }
