import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { CardComponent } from './card/card.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { LineChartsComponent } from './line-charts/line-charts.component';
import { DashboardService } from './dashboard.service';
import { ChartsModule } from 'ng2-charts';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  imports: [
    ChartsModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [DashboardComponent,CardComponent,PieChartComponent,LineChartsComponent],
  providers:[DashboardService]
})
export class DashboardModule {}
