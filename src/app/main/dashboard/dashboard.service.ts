import { Injectable } from "@angular/core";
import { NetworkService } from "../../shared/network.service";

@Injectable()
export class DashboardService {
  constructor(private _networkService: NetworkService) { }

  getDashboardCount() {
    return this._networkService.get("api/dashboard", null, null, "bearer");
  }

  getUserMonthlyApi() {
    return this._networkService.get(
      "api/dashboard/monthlyUserData",
      null,
      null,
      "bearer"
    );
  }

  getUserPieChartDataApi() {
    return this._networkService.get("api/dashboard/userData")
  }

  getProjectPieChartDataApi() {
    return this._networkService.get("api/dashboard/projectData")
  }

  getRacePieChartDataApi() {
    return this._networkService.get("api/dashboard/raceData")
  }

  getProjectLineChartDataApi() {
    return this._networkService.get("api/dashboard/monthlyProjectData")
  }

  getMonthlyRaceApi() {
    return this._networkService.get(
      "api/dashboard/monthlyRaceData",
      null,
      null,
      "bearer"
    );
  }

  getFundingPieChartDataApi () {
    return this._networkService.get(
      "api/dashboard/fundingData",
      null,
      null,
      "bearer"
    );
  }

}
