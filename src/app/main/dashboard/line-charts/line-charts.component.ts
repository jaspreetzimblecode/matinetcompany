import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { ThemeService } from 'ng2-charts';

@Component({
  selector: 'app-line-charts',
  templateUrl: './line-charts.component.html',
  styleUrls: ['./line-charts.component.scss']
})
export class LineChartsComponent implements OnInit {
  lineChartOptions: any;
  lineChartLegend: boolean;
  lineChartType: string;
  chartReady: boolean = false;

  lineChartUserData: any;
  lineChartUserLabels: any;
  lineChartUserColour: any;

  lineChartProjectData: any;
  lineChartProjectLabels: any;
  lineChartTransactionColour: any;

  lineChartRaceData:any;
  lineChartRaceLabels: any;
  lineChartCallsColour: any;

  userMonthlyData: {};
  projectLineChartData: {};
  monthlyRaceData:{};

  constructor(private dashboardService: DashboardService,private themeService: ThemeService) { }

  ngOnInit() {
    this.getUserMonthlyData();
  //  this.getRaceMonthlyData();
    // this.getProjectLineChartData1();
  }
  confLineGraph() {
    this.lineChartOptions = {
      animation: false,
      responsive: true
    };
    this.lineChartUserColour = [
      {
        backgroundColor: 'rgba(0, 153, 153,0.6)',
        borderColor: 'rgba(148,159,177,1)',
        pointBackgroundColor: 'rgba(148,159,177,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(148,159,177,0.8)'
      }];

    this.lineChartCallsColour = [
      { 
        backgroundColor: 'rgba(102, 102, 102,0.7)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      }
    ]
    this.lineChartTransactionColour = [
      { 
        backgroundColor: 'rgb(112, 219, 112,0.6)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)'
      }
    ]
    this.lineChartLegend = true;
    this.lineChartType = 'line';
  }

  getRaceMonthlyData() {
    this.dashboardService.getMonthlyRaceApi().subscribe(res => {
      if (!res["msg"]) {
        this.monthlyRaceData = res;
        console.log(this.monthlyRaceData)
        let raceData = [];
        let raceLabelDate = [];
        for (let i = 0; i < res[0].series.length; i++) {
          raceData.push(res[0].series[i].value);
          raceLabelDate.push(res[0].series[i].name)
        }
        this.lineChartRaceData = [
          {
            data: raceData,
            label: "Total Races"
          }
        ]
        this.lineChartRaceLabels = raceLabelDate;
        this.confLineGraph();
      }
    });
  }
  
  getProjectLineChartData1() {
    this.dashboardService.getProjectLineChartDataApi().subscribe(res => {
      if (!res["msg"]) {
        let projectData = [];
        let projectLabelDate = [];
        this.projectLineChartData = res;
        for (let i = 0; i < res[0].series.length; i++) {
          projectData.push(res[0].series[i].value);
          projectLabelDate.push(res[0].series[i].name)
        }
        this.lineChartProjectData = [
          {
            data: projectData,
            label: "Total Projects"
          }
        ]
        this.lineChartProjectLabels = projectLabelDate
        this.confLineGraph();
      }
    })
  }

  getUserMonthlyData() {
    this.chartReady = true
    this.dashboardService.getUserMonthlyApi().subscribe(res => {
      if (!res["msg"]) {
        this.userMonthlyData = res;
        let userData = [];
        let userLabelDate = [];
        for (let i = 0; i < res[0].series.length; i++) {
          userData.push(res[0].series[i].value);
          userLabelDate.push(res[0].series[i].name)
        }
        this.lineChartUserData = [
          {
            data: userData,
            label: "Total Users"
          }
        ]
        this.lineChartUserLabels = userLabelDate
        this.confLineGraph();
      }
    });
  }
  
}
