import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../dashboard.service';
import { CommonService } from '../../../shared/common.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  totalUsers;
  localStore ;
  totalCompanies;
  totalSchool;
  totalPlayList;
  isLoading:boolean = false;
  constructor(private dashboardService:DashboardService,private commonservice:CommonService) { }

  ngOnInit() {
    this.localStore = this.commonservice.getUser();
      this.getDashboardCount();

  }

  // dashboard cards 
  getDashboardCount() {
    this.isLoading = true;
    this.dashboardService.getDashboardCount().subscribe(res => {
      this.isLoading = false;
      if ((res["message"] = "Success")) {
        this.totalUsers = res["data"]["totalUsers"];
        this.totalCompanies= res["data"]["totalCompanies"];
        this.totalSchool = res["data"]["totalSchool"];  
        this.totalPlayList = res["data"]["totalPlayList"];
        totalCompanies: 15
totalPlayList: 15
totalSchool: 40
totalUsers: 7
      }
    });
  }
// dashboard cards end 
}
