import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StreamService } from './stream.service';
import { UploadService } from './upload.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { MatSnackBar } from "@angular/material";
import { Router } from '@angular/router';
@Component({
  selector: 'app-upload-vedio',
  templateUrl: './upload-vedio.component.html',
  styleUrls: ['./upload-vedio.component.css']
})
export class UploadVedioComponent implements OnInit {
  videoDur:any;
  selectedFiles: FileList;
  fileType:string;
  fileSize:string;
  isUpload:boolean = false;
  isLoading:boolean = false;
  vedioForm: FormGroup;
  styleList: Array<object> = [];
  musicList: Array<object> = [];
  hashtagList: Array<object> = [];
  songsList: Array<object> = [];
  venueList: Array<object> = [];
  choreographerList:Array<object> = [];
  composerList:Array<object> = [];
  dancerList:Array<object> = [];
  genreList:Array<object> = [];

  constructor(
    private uploadService:UploadService,
    private streamService:StreamService,
    private router: Router,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.inilialForm();
    this.getGenreList();
    this.getMusicList();
    this.getStyleList();
  }
  /* genre */
  onGenreSelected (event) {
    console.log(this.vedioForm.value)
  } 
  onGenreItemAdded(event) {
    console.log(this.vedioForm.value)
  }
  getGenreList() {
    //var queryValue = this.queryParams();
    this.isLoading = true;
    this.uploadService.getGenreListApi().subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.genreList = res["data"]["genreList"];
          console.log(this.genreList)
        } else {
          this.genreList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
 /* genre end */

   /* style */
   onStyleSelected (event) {
    console.log(this.vedioForm.value)
  } 
  onStyleItemAdded(event) {
    console.log(this.vedioForm.value)
  }
  getStyleList() {
    //var queryValue = this.queryParams();
    this.isLoading = true;
    this.uploadService.getStyleListApi().subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.styleList = res["data"]["styleList"];
          console.log(this.styleList,"STyle")
        } else {
          this.styleList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
 /* style end */

   /* music */
   onMusicSelected (event) {
    console.log(this.vedioForm.value)
  } 
  onMusicItemAdded(event) {
    console.log(this.vedioForm.value)
  }
  getMusicList() {
    //var queryValue = this.queryParams();
    this.isLoading = true;
    this.uploadService.getMusicListApi().subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.musicList = res["data"]["musicList"];
          console.log(this.musicList,"Music")
        } else {
          this.musicList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
 /* style end */

 /* hashtags */
onHashtagSelected (event) {
  console.log(this.vedioForm.value)
} 
onHashtagItemAdded(event) {
  console.log(this.vedioForm.value)
}
onHashtagTextChange(event) {
  if(event.length >= 3) {
    let query=event.trim();
      this.isLoading = true;
      this.uploadService.getHashtagListApi(query).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.hashtagList = res["data"]["suggestionList"];
            console.log(this.hashtagList,"HASHTAGS")
          } else {
            this.hashtagList = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }
}
/* hashtags end */
  
/* songs */
onSongSelected (event) {
    console.log(this.vedioForm.value)
  } 
  onSongItemAdded(event) {
    console.log(this.vedioForm.value)
  }
  onSongTextChange(event) {
    if(event.length >= 3) {
      let query=event.trim();
        this.isLoading = true;
        this.uploadService.getSongListApi(query).subscribe(
          res => {
            this.isLoading = false;
            if ((res["message"] = "Success")) {
              this.songsList = res["data"]["suggestionList"];
              console.log(this.songsList,"SONGS")
            } else {
              this.songsList = [];
            }
            this.isLoading = false;
          },
          err => {
            this.isLoading = false;
          }
        );
      }
  }
  /* songs end */

  /* venue */
onVenueSelected (event) {
  console.log(this.vedioForm.value)
} 
onVenueItemAdded(event) {
  console.log(this.vedioForm.value)
}
onVenueTextChange(event) {
  if(event.length >= 3) {
    let query=event.trim();
      this.isLoading = true;
      this.uploadService.getVenueListApi(query).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.venueList = res["data"]["suggestionList"];
            console.log(this.venueList,"Venue")
          } else {
            this.venueList = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }
}
/* venue end */

  /* choreographer */
  onChoreographerSelected (event) {
    console.log(this.vedioForm.value)
  } 
  onChoreographerItemAdded(event) {
    console.log(this.vedioForm.value)
  }
  onChoreographerTextChange(event) {
    if(event.length >= 3) {
      let query=event.trim();
        this.isLoading = true;
        this.uploadService.getChoreographerListApi(query).subscribe(
          res => {
            this.isLoading = false;
            if ((res["message"] = "Success")) {
              this.choreographerList = res["data"]["suggestionList"];
              console.log(this.choreographerList,"choreographerList")
            } else {
              this.choreographerList = [];
            }
            this.isLoading = false;
          },
          err => {
            this.isLoading = false;
          }
        );
      }
  }
  /* choreographer end */

    /* composer */
    onComposerSelected (event) {
      console.log(this.vedioForm.value)
    } 
    onComposerItemAdded(event) {
      console.log(this.vedioForm.value)
    }
    onComposerTextChange(event) {
      if(event.length >= 3) {
        let query=event.trim();
          this.isLoading = true;
          this.uploadService.getComposerListApi(query).subscribe(
            res => {
              this.isLoading = false;
              if ((res["message"] = "Success")) {
                this.composerList = res["data"]["suggestionList"];
                console.log(this.composerList,"Composer")
              } else {
                this.composerList = [];
              }
              this.isLoading = false;
            },
            err => {
              this.isLoading = false;
            }
          );
        }
    }
    /* composer end */


     /* dancer */
     onDancerSelected (event) {
      console.log(this.vedioForm.value)
    } 
    onDancerItemAdded(event) {
      console.log(this.vedioForm.value)
    }
    onDancerTextChange(event) {
      if(event.length >= 3) {
        let query=event.trim();
          this.isLoading = true;
          this.uploadService.getDancerListApi(query).subscribe(
            res => {
              this.isLoading = false;
              if ((res["message"] = "Success")) {
                this.dancerList = res["data"]["suggestionList"];
                console.log(this.dancerList,"dancer")
              } else {
                this.dancerList = [];
              }
              this.isLoading = false;
            },
            err => {
              this.isLoading = false;
            }
          );
        }
    }
    /* dancer end */



  inilialForm() {
    this.vedioForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      hashtag: ["", [Validators.required]],
      song: ["", [Validators.required]],
      venue: ["", [Validators.required]],
      description: ["", [Validators.required]],
      thumbnail: [""],
      url: [""],
      genre: [[], [Validators.required]],
      music: [[], [Validators.required]],
      style : [[], [Validators.required]],
      choreographer: ["", [Validators.required]],
      composer: ["", [Validators.required]],
      dancer: [[], [Validators.required]],
      highlightList: [[]],
    });
    console.log(this.vedioForm.value)
  }
  upload() {
    const file = this.selectedFiles.item(0);
    this.uploadService.uploadFile(file);
    this.isUpload = true;
    }
    
    selectFile(event) {
      localStorage.removeItem('total');
      localStorage.removeItem('remaining');
      localStorage.removeItem('fileUrl');
    this.selectedFiles = event.target.files;
this.getVedioDuration();
    console.log(this.selectedFiles)
    var totalBytes = this.selectedFiles[0].size;
    this.fileType = this.selectedFiles[0].type;
    if(totalBytes < 1000000){
      this.fileSize  = Math.floor(totalBytes/1000) + ' KB';
        ;
    }else{
       this.fileSize = Math.floor(totalBytes/1000000) + ' MB';  
       
    }
    console.log(this.fileSize)
    }
    hastag = [];
    // videoDur:any;
    getVedioDuration() {
      this.hastag.push(this.selectedFiles[0]);
      var video = document.createElement('video');
      video.preload = 'metadata';
      video.onloadedmetadata = function() {
      window.URL.revokeObjectURL(video.src);
        let duration = video.duration;
       console.log('get duration',duration);
       localStorage.setItem('dur',duration.toString());

       // updateInfos();
      }
      video.src = URL.createObjectURL(this.selectedFiles[0]);
    }

    readLocalStorageValue(key: any): any {
     let total  = localStorage.getItem('total');
     let remain = localStorage.getItem('remaining');
     return this.percentage(remain,total) ;

    }
   percentage(partialValue, totalValue) {
      return (100 * partialValue) / totalValue;
   }

   submitForm() {
     let url = localStorage.getItem("fileUrl")
     let duration = localStorage.getItem("dur")
     console.log(url)
     console.log(this.vedioForm.value,"FORM")
     let hashtagFinal=[];
     let songsFinal=[];
     let venueFinal=[];
     let genreFinal=[];
     let musicFinal=[];
     let choreographerFinal=[];
     let composerFinal=[];
     let dancerFinal=[];
     let styleFinal=[];

     let req:any;
      req = this.vedioForm.value;
    
      req['hashtag'].forEach(element => {
        hashtagFinal.push(element['suggestionText'])
      });
      req['song'].forEach(element => {
        songsFinal.push(element['suggestionText'])
      });
      req['venue'].forEach(element => {
        venueFinal.push(element['suggestionText'])
      });
      req['genre'].forEach(element => {
        genreFinal.push(element['suggestionText'])
      });
      req['music'].forEach(element => {
        musicFinal.push(element['suggestionText'])
      });
      req['choreographer'].forEach(element => {
        choreographerFinal.push(element['suggestionText'])
      });
      req['composer'].forEach(element => {
        composerFinal.push(element['suggestionText'])
      });
      req['dancer'].forEach(element => {
        dancerFinal.push(element['suggestionText'])
      });
      req['style'].forEach(element => {
        styleFinal.push(element['suggestionText'])
      });

      req['hashtag'] = hashtagFinal;
      req['song'] = songsFinal;
      req['venue'] = venueFinal;
      req['genre'] = genreFinal;
      req['music'] = musicFinal;
      req['choreographer'] = choreographerFinal;
      req['composer'] = composerFinal;
      req['dancer'] = dancerFinal;
      req['style'] = styleFinal;
      req['duration'] = duration;
      req['url'] = url;
      req['thumbnail'] = "kdadmas";
    console.log(req,"FinalReq")
    this.postVideo(req)
   }

   postVideo(req) {
    this.uploadService.postVideoApi(req).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this._snackBar.open("Video uploaded successfully!","",{
                        duration: 5000,
                        horizontalPosition:'right',
                        verticalPosition:'top',
                        panelClass: ['success']
                      });
                      this.vedioForm.reset();
                      this.router.navigate(['/main/video/view-video']);
                      localStorage.removeItem("fileUrl")
                      localStorage.removeItem("dur")
                      localStorage.removeItem("total")
                      localStorage.removeItem("remaining")
        } else {
          this.choreographerList = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }
  
}
