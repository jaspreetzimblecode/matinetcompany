import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { UploadRoutes } from './upload.routing';
import { SharedModule } from '../../shared/shared.module';
import { UploadService } from './upload.service';
import { UploadVedioComponent } from './upload-vedio.component';
import {ProgressBarModule} from "angular-progress-bar"
import { StreamService } from './stream.service';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(UploadRoutes),
    ProgressBarModule,
    NgMultiSelectDropDownModule.forRoot(),
    TagInputModule
    
  ],
  schemas:[NO_ERRORS_SCHEMA],
  declarations: [UploadVedioComponent],
  providers:[UploadService,StreamService]
})
export class UploadVedioModule {}
