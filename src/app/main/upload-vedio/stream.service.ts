import { Injectable } from '@angular/core';

import { Subject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StreamService {

  constructor() { }
  alertChannel: Subject<any> = new Subject();
  alertSubscription$ = this.alertChannel.asObservable();
  
  streamMessage(data: any) {
    let obj = {data:data}
    this.alertChannel.next(obj);
  }

  getuploadData(): Observable<any> {
    return this.alertSubscription$;
  }
}
