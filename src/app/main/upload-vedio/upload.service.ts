import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { StreamService } from './stream.service';
import { NetworkService } from '../../shared/network.service';
@Injectable({
  providedIn: 'root'
})
export class UploadService {
  FOLDER="Matinet/";
  constructor( public streamService:StreamService,private _networkService: NetworkService) { }

  getGenreListApi() {
    return this._networkService.get(
      "api/genre" ,
      null,
      null,
      "bearer"
    );
  }
  getStyleListApi() {
    return this._networkService.get(
      "api/style?offset=0&limit=200" ,
      null,
      null,
      "bearer"
    );
  }
  getMusicListApi() {
    return this._networkService.get(
      "api/music" ,
      null,
      null,
      "bearer"
    );
  }

  getHashtagListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=hashtag&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }
  getSongListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=song&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }
  getVenueListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=venue&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }

  getChoreographerListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=choreographer&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }

  getComposerListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=composer&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }

  getDancerListApi(query:any) {
    return this._networkService.get(
      "api/playList/suggestion?type=dancer&"+"keyword="+query ,
      null,
      null,
      "bearer"
    );
  }
  uploadFile(file) {
    const contentType = file.type;
    const bucket = new S3(
          {
              accessKeyId: 'AKIA3GYPYSK46DD5HCEO',
              secretAccessKey: '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',
              region: 'us-east-2'
          }
      );
      const params = {
          Bucket: 'matinet',
          Key: this.FOLDER + file.name,
          Body: file,
          ACL: 'public-read',
            
          ContentType: contentType
      };
      const options = {
        partSize: 10 * 1024 * 1024,
        queueSize: 1,
      }
    //   bucket.upload(params, function (err, data) {
    //       if (err) {
    //           console.log('There was an error uploading your file: ', err);
    //           return false;
    //       }
    //       console.log('Successfully uploaded file.', data);
    //       return true;
    //   });
//for upload progress   



bucket.upload(params,options).on('httpUploadProgress', function (evt) {
      console.log(evt.loaded + ' of ' +evt.total + ' bytes');
        localStorage.setItem('remaining',evt.loaded.toString());
        localStorage.setItem('total',evt.total.toString())
      }).send(function (err, data) {
          console.log(err,data,'------------')
          if (err) {
              console.log('There was an error uploading your file: ', err);
              return false;
          }
          localStorage.setItem('fileUrl',data.Location);
          return true;
      });
}

postVideoApi(body:any) {
  return this._networkService.post(
    "api/video",
    body,
    null,
    "bearer"
  );
}

}
