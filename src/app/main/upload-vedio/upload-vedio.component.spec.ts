import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVedioComponent } from './upload-vedio.component';

describe('UploadVedioComponent', () => {
  let component: UploadVedioComponent;
  let fixture: ComponentFixture<UploadVedioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadVedioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVedioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
