import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class DistanceService {

  constructor(private _networkService:NetworkService) { }

  getDistanceListApi(query:any) {
    return this._networkService.get(
      "api/distance?" + query,
      null,
      null,
      "bearer"
    );
  }

  postDistanceListApi(body:any) {
    return this._networkService.post(
      "api/distance",
      body,
      null,
      "bearer"
    );
  }
  
  putDistanceListApi(body:any) {
    return this._networkService.put(
      "api/distance",
      body,
      null,
      "bearer"
    );
  }
  deleteDistanceListApi(id:any) {
    return this._networkService.delete(
      "api/distance/"+id,
      null,
      null,
      "bearer"
    );
  }

    
  uploadImage(image: File) {
    const formData = new FormData();
    formData.append("image", image);
    return this._networkService.uploadImages("api/s3upload/image-upload-sync", formData, null, "bearer");
  }
  
}
