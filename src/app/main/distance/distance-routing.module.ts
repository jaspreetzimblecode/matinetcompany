import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DistanceManagementComponent } from './distance-management.component';
import { ViewDistanceComponent } from './view-distance/view-distance.component';


const routes: Routes = [
  {path:"",component:DistanceManagementComponent,
children:[
  { path: '', redirectTo: '/main/distance-management/view-distance', pathMatch: 'full' },
  {path:"view-distance",component:ViewDistanceComponent}
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DistanceRoutingModule { }
