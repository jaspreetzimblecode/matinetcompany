import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { DistanceService } from '../distance.service';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { RuleService } from '../../rules/rule.service';

@Component({
  selector: 'app-view-distance',
  templateUrl: './view-distance.component.html',
  styleUrls: ['./view-distance.component.css']
})
export class ViewDistanceComponent implements OnInit {
  isadd:boolean = false;
  rowDetail:any;
  selectedfile: any;
  btntext="button"
  query: string;
  title = "";
  showimg:string = "";
  statusFormSubmitted: boolean = false;
  modalForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
// filters end

list = [];
ruleList = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private distanceService:DistanceService,
    private ruleService:RuleService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    // this.getCategoryList();
    this.getDistanceList();
    this.getRuleList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)

  getDistanceList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.distanceService.getDistanceListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.list = res["data"]["distanceList"];
          console.log(this.list)
        } else {
          this.list = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

    getRuleList() {
      let queryParam = "";
      var queryValue = this.queryParams();
      this.isLoading = true;
      this.ruleService.getRuleListApi(queryValue).subscribe(
        res => {
          this.isLoading = false;
          if ((res["message"] = "Success")) {
            this.ruleList = res["data"]["rulesList"];
       console.log(this.ruleList)
            console.log(this.list)
          } else {
            this.ruleList = [];
          }
          this.isLoading = false;
        },
        err => {
          this.isLoading = false;
        }
      );
    }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getDistanceList();
  }

  searchFiltersBtn() {
    this.getDistanceList();
  }


  prepareUpdateStatusForm() {
    this.modalForm = this.formBuilder.group({
      distanceId: ["", [Validators.required]],
      distance: ["", [Validators.required]],
      sortOrder: ["", [Validators.required]],
      ruleId: ["", [Validators.required]],      
    });
  }
  
  async addImages() {
    const { value: file } = await Swal.fire({
      title: 'Select image',
      input: 'file',
      inputAttributes: {
        'accept': 'image/*',
        'aria-label': 'Upload Images'
      }
    })
    if (file) {
      this.selectedfile = file;
      this.onFileChanged(file);
    }
  }

  onFileChanged(event) {
   this.isLoading = true;
   
       this.distanceService.uploadImage(this.selectedfile).subscribe(res => {
      if (res["message"] === "Success") {
        this.isLoading = false;
          this.showimg = res.data.url;

          console.log(this.showimg,"showImg")
          this.selectedfile = null;
      } else {
        this.isLoading = false;
        Swal.fire({
          title: "Error",
          text: res.message
        });
      }
    }, err => {
    });
  }

  openModal(row, content, btn,value) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md'
    };
    this.prepareUpdateStatusForm();
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
if(value =='add') {
  console.log("add");
  this.title = "Add distance"
}if (value =='edit') {
  console.log(row)
  this.title = "Edit distance"
  this.modalForm.controls["distanceId"].setValue(row["distanceId"]);
  this.modalForm.controls["sortOrder"].setValue(row["sortOrder"]);
  this.modalForm.controls["distance"].setValue(row["distanceDisplay"]);
  this.modalForm.controls["ruleId"].setValue(row["ruleId"]);
  this.showimg = row['icon'];
  console.log('edit')
  this.rowDetail = row;
}
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.title = "";
        this.showimg = "";
        this.rowDetail = "";
        this.modalForm.reset();
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  postCategory() {
      this.isLoading = true ;
      let finalObj = 
        this.modalForm.value;
      finalObj['icon'] = this.showimg;
      console.log(finalObj,"finalIbj")
      if(this.title == 'Add distance') {
        this.addDistance(finalObj);
        } if(this.title == 'Edit distance') {
       this.editCategory(finalObj);
        }
        this.title = "";
        err => {
          this.isLoading = false;
          this.modalRef.close();
          this.modalForm.reset();
          this.showimg = "";
        }
  }

  deleteImg() {
    this.showimg = "";
  }
  addDistance (finalObj) {
    console.log(finalObj,"ppost")
    this.distanceService.postDistanceListApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
            this._snackBar.open("Distance created successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            })
            this.getDistanceList();
            this.modalRef.close();
        } else if(res["message"] == "Failure"){
          this._snackBar.open(res.data,"",{
            duration: 5000,
            horizontalPosition:'right',
            verticalPosition:'top',
            panelClass: ['success']
          });
        }
      },err => {
        console.log("error")
        this.isLoading = false;
      })
  }

  editCategory (finalObj) {
    this.modalForm.value;
    this.distanceService.putDistanceListApi(finalObj).subscribe(
      res => {
        this.isLoading = false;
        if (res["message"] == "Success") {
            this._snackBar.open("Distance updated successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            });
            this.getDistanceList();
            this.modalRef.close();
        } 
      })
  }

  deleteCategoryAction(item) {
    Swal.fire({
      title: "You want to delete: " + item,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(result => {
      if (result.value) {
        console.log(result.value)
        this.isLoading = true;
        this.distanceService.deleteDistanceListApi(item).subscribe(
          res => {
            this.isLoading = false;
            if (res["message"] == "success") {
              this._snackBar.open("Distance deleted successfully!","",{
                duration: 5000,
                horizontalPosition:'right',
                verticalPosition:'top',
                panelClass: ['success']
              });
            } else {
              Swal.fire("Error", res.data, "error");
            }
            this.isLoading = false;
            this.getDistanceList();
          },
          err => {
            this.isLoading = false;
          }
        );
      }
    });
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
