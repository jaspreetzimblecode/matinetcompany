import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DistanceRoutingModule } from './distance-routing.module';
import { DistanceManagementComponent } from './distance-management.component';
import { ViewDistanceComponent } from './view-distance/view-distance.component';
import { DistanceService } from './distance.service';
import { SharedModule } from '../../shared/shared.module';
import { RuleService } from '../rules/rule.service';


@NgModule({
  declarations: [DistanceManagementComponent, ViewDistanceComponent],
  imports: [
    CommonModule,
    DistanceRoutingModule,
    SharedModule
  ]
  ,providers:[DistanceService,RuleService]
})
export class DistanceModule { }
