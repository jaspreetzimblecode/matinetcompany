import { Component, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-resetbtn',
  templateUrl: './resetbtn.component.html',
  styleUrls: ['./resetbtn.component.scss']
})
export class ResetbtnComponent implements OnInit {
@Output() resetfn = new EventEmitter; 
  constructor() { }

  ngOnInit() {
  }

}
