import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { ParticipantService } from './participant.service';

@Component({
  selector: 'app-view-participant',
  templateUrl: './view-participant.component.html',
  styleUrls: ['./view-participant.component.css']
})
export class ViewParticipantComponent implements OnInit {
  isadd:boolean = false;
  rowDetail:any;
  selectedfile: any;
  btntext="button"
  query: string;
  title = "";
  showimg:any;
  statusFormSubmitted: boolean = false;
  modalForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
// filters end

list = [];
ruleList = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private participantService:ParticipantService,
    
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    // this.getCategoryList();
    this.getDistanceList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)

  getDistanceList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.participantService.getParticipantListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.list = res["data"]["participantList"];
          console.log(this.list)
        } else {
          this.list = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getDistanceList();
  }

  searchFiltersBtn() {
    this.getDistanceList();
  }


  prepareUpdateStatusForm() {
    this.modalForm = this.formBuilder.group({
      distanceId: ["", [Validators.required]],
      distance: ["", [Validators.required]],
      sortOrder: ["", [Validators.required]],
      ruleId: ["", [Validators.required]],      
    });
  }

  openModal(row, content, btn,value) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md'
    };
    this.prepareUpdateStatusForm();
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
if(value =='add') {
  console.log("add");
  this.title = "Add distance"
}if (value =='edit') {
  console.log(row)
  this.title = "Edit distance"
  this.modalForm.controls["distanceId"].setValue(row["distanceId"]);
  this.modalForm.controls["sortOrder"].setValue(row["sortOrder"]);
  this.modalForm.controls["distance"].setValue(row["distanceDisplay"]);
  this.modalForm.controls["ruleId"].setValue(row["ruleId"]);
  this.showimg = row['icon'];
  console.log('edit')
  this.rowDetail = row;
}
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.title = "";
        this.showimg = "";
        this.rowDetail = "";
        this.modalForm.reset();
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
