import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParticipantManagementComponent } from './participant-management.component';
import { ViewParticipantComponent } from './view-participant.component';

const routes: Routes = [
  {path:"",component:ParticipantManagementComponent,
children:[
  { path: '', redirectTo: '/main/participant-management/view-participant', pathMatch: 'full' },
  {path:"view-participant",component:ViewParticipantComponent}
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParticipantRoutingModule { }
