import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParticipantRoutingModule } from './participant-routing.module';
import { ParticipantManagementComponent } from './participant-management.component';
import { ViewParticipantComponent } from './view-participant.component';
import { ParticipantService } from './participant.service';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [ParticipantManagementComponent, ViewParticipantComponent],
  imports: [
    CommonModule,
    ParticipantRoutingModule,
    SharedModule
  ],
  providers:[ParticipantService]
})
export class ParticipantModule { }
