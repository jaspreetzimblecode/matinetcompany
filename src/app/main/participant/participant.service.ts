import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class ParticipantService {


  constructor(private _networkService:NetworkService) { }

  getParticipantListApi(query:any) {
    return this._networkService.get(
      "api/participant?" + query,
      null,
      null,
      "bearer"
    );
  }
}
