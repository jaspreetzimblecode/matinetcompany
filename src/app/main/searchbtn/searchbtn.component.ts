import { Component, OnInit, Output,EventEmitter, Input } from '@angular/core';


@Component({
  selector: 'app-searchbtn',
  templateUrl: './searchbtn.component.html',
  styleUrls: ['./searchbtn.component.scss']
})
export class SearchbtnComponent implements OnInit {
  @Output() searchfn = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

}
