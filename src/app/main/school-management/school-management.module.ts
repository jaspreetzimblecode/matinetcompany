import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolManagementRoutingModule } from './school-management-routing.module';
import { ViewSchoolComponent } from './view-school.component';
import { SchoolManagementComponent } from './school-management/school-management.component';
import { SharedModule } from '../../shared/shared.module';
import { PendingSchoolComponent } from './pending-school/pending-school.component';

@NgModule({
  declarations: [ViewSchoolComponent, SchoolManagementComponent, PendingSchoolComponent],
  imports: [
    CommonModule,
    SchoolManagementRoutingModule,
    SharedModule
  ]
})
export class SchoolManagementModule { }
