import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModalOptions, NgbModalRef, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBar } from '@angular/material';
import Swal from 'sweetalert2';
import { SchoolService } from '../school.service';

@Component({
  selector: 'app-pending-school',
  templateUrl: './pending-school.component.html',
  styleUrls: ['./pending-school.component.css']
})
export class PendingSchoolComponent implements OnInit {
  userDetails;
  rowDetail:any;
  btntext="button"
  query: string;
  statusFormSubmitted: boolean = false;
  statusForm: FormGroup;
  statusList: Array<object> = [
    { id: "active", name: "Active" },
    { id: "inactive", name: "Inactive" },
    { id: "blocked", name: "Block" }
  ];

  // END - Update user variable
  // filters
  searchStartDate: string;
  searchEndDate: string;
  searchUserStatus: string = "all";
  searchBy: string = "all";
  searchText: string;
// filters end

List = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  userSearchType: number = 0;
  minDate: any;
  maxDate: any;
  isLoading: boolean=false;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private schoolService: SchoolService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getSchoolList();
  }

  queryParams() {
    let query = "";
    if (this.searchUserStatus && this.searchUserStatus != "all") query = query + "status=" + this.searchUserStatus+"&";
    
    if (this.searchBy && this.searchBy == "mobile" && this.searchText && this.searchText !== "") {
      query = query + "mobile=" + this.searchText.trim()+"&";
    }
    if (this.searchBy && this.searchBy == "email" && this.searchText && this.searchText !== "") {
      query = query + "email=" + this.searchText.toLowerCase().trim()+"&";
    }
    if (this.searchStartDate && this.searchStartDate != "") {
      // convert to epoch time
      let startDate = new Date(this.searchStartDate);
      query = query + "startDate=" + Math.floor(startDate.getTime() / 1000);
    }
    if (this.searchEndDate && this.searchEndDate != "") {
      // convert to epoch time
      let endDate = new Date(this.searchEndDate);
      endDate.setDate(endDate.getDate() + 1);
      query = query + "&endDate=" + Math.floor(endDate.getTime() / 1000);
    }
    return query;
  }
  // GET USERS (By Default ALL)
  getSchoolList() {
    let queryParam = "";
    var queryValue = this.queryParams();
    this.isLoading = true;
    this.schoolService.getPendingSchoolListApi(queryValue).subscribe(
      res => {
        this.isLoading = false;
        if ((res["message"] = "Success")) {
          this.List = res["data"]["schoolList"];
          console.log(this.List)
        } else {
          this.List = [];
        }
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
      }
    );
  }

  onReset() {
    this.isLoading = true;
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.isLoading= false;
    }, 2000);
    this.searchUserStatus = "",
    this.searchText = "",
    this.searchBy ="",
      (this.searchStartDate = ""),
      (this.searchEndDate = "");
    this.getSchoolList();
  }

  searchFiltersBtn() {
    this.getSchoolList();
  }



  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      status: ["", [Validators.required]]
    });
  }
  
  updateUserStatusModal(row,content, btn) {
    this.modelOptions = {
      backdrop: "static",
      keyboard: false,
      size:'md'
    };
    this.rowDetail = row;
    console.log(this.rowDetail)
    btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();

    this.prepareUpdateStatusForm();
    this.modalRef = this.modalService.open(content, this.modelOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateUserStatus(test) {

    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.isLoading = true ;
      let finalObj = {
        companyId: this.rowDetail["id"],
        status: this.statusForm.controls.status.value
      };
      this.schoolService.updateSchoolStatus(finalObj).subscribe(
        res => {
          this.isLoading = false;
          if (res["message"] == "Success") {
            this._snackBar.open("School status updated successfully!","",{
              duration: 5000,
              horizontalPosition:'right',
              verticalPosition:'top',
              panelClass: ['success']
            });
            // Swal.fire("Success", "Officer updated successfully!", "success");
            this.getSchoolList();
            this.modalRef.close();
            this.statusForm.reset();
          
          } else {
          }
        },
        err => {
          this.isLoading = false;
        }
      );
    }
  }

  swalImage (image) {
    Swal.fire({
      imageUrl: image,
      imageWidth: 400,
      imageHeight: 400,
    })
  }
  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}
