import { Injectable } from '@angular/core';
import { NetworkService } from '../../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class SchoolService {

  constructor(private _networkService: NetworkService) {}

  getActiveSchoolListApi(query) {
    return this._networkService.get(
      "api/school?status=accepted?" + query,
      null,
      null,
      "bearer"
    );
  }
  getPendingSchoolListApi(query) {
    return this._networkService.get(
      "api/school?status=pending?" + query,
      null,
      null,
      "bearer"
    );
  }
  updateSchoolStatus(body: any) {
    return this._networkService.put("api/school/",body, null, "bearer");
  }
}
