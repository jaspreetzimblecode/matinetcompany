import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchoolManagementComponent } from './school-management/school-management.component';
import { ViewSchoolComponent } from './view-school.component';
import { PendingSchoolComponent } from './pending-school/pending-school.component';


const routes: Routes = [
  {path:"",component:SchoolManagementComponent,
children:[
  { path: '', redirectTo: '/main/school-management/pending-school', pathMatch: 'full' },
  {path:"active-school",component:ViewSchoolComponent},
  {path:"pending-school",component:PendingSchoolComponent}
]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolManagementRoutingModule { }
