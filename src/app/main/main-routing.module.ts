import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { FullComponent } from '../layouts/full/full.component';
// import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardMain } from '../auth/auth-guard.service';
import { DashboardComponent } from './dashboard/dashboard.component';


const routes: Routes = [
  // { path: "", redirectTo: "/main/dashboard", pathMatch: "full" },
  {
    path: "main",
    component: MainComponent,
    canActivate: [AuthGuardMain],
    children: [
      { path: "", redirectTo: "/main/upload-vedio", pathMatch: "full" },
      // {
      //   path: "dashboard",
      //   component: DashboardComponent,
      // },
      { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)},
      { path: 'school-management', loadChildren: () => import('../main/school-management/school-management.module').then(m => m.SchoolManagementModule)},
      { path: 'upload-vedio', loadChildren: () => import('../main/upload-vedio/upload.module').then(m => m.UploadVedioModule)},
      { path: 'video', loadChildren: () => import('../main/myvideo/myvideo.module').then(m => m.MyvideoModule)},
      { path: 'create', loadChildren: () => import('../main/create-playlist/create-playlist.module').then(m => m.CreatePlaylistModule)},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
