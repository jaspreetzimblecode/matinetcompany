import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../shared/common.service';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { NgbModal, NgbModalRef, NgbModalOptions, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import { MatSnackBar } from "@angular/material";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['../full.component.scss']
})
export class AppHeaderComponent implements OnInit{
  userData:any;
  statusForm: FormGroup;
  modelOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  isLoading: boolean=false;
  closeResult: string;
  private modalRef: NgbModalRef;

  constructor(private commonService: CommonService,
    private modalService: NgbModal,
    private _snackBar: MatSnackBar,
    private formBuilder: FormBuilder) { }
  
  ngOnInit(
    
  ) {
    this.userData = this.commonService.getUser();
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
  }
  
  // updateUserStatusModal(content, btn) {
  //   this.modelOptions = {
  //     backdrop: "static",
  //     keyboard: false,
  //     size:'md'
  //   };
  //   btn && btn.parentElement && btn.parentElement.parentElement && btn.parentElement.parentElement.blur();
  //   this.prepareUpdateStatusForm();
  //   this.modalRef = this.modalService.open(content, this.modelOptions);
  //   this.modalRef.result.then(
  //     result => {
  //       this.closeResult = `Closed with: ${result}`;
  //     },
  //     reason => {
  //       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //     }
  //   );
  // }

  // updateUserStatus(test) {
    
  //   if (this.statusForm.valid) {
  //     this.isLoading = true ;
  //     let finalObj = {
  //       email: this.statusForm.controls.email.value,
  //       password: this.statusForm.controls.password.value
  //     };
  //     this.userMgmtService.createAdminApi(finalObj).subscribe(
  //       res => {
  //         this.isLoading = false;
  //         if (res["message"] == "Success") {
  //           this._snackBar.open("Admin created successfully!","",{
  //             duration: 5000,
  //             horizontalPosition:'right',
  //             verticalPosition:'top',
  //             panelClass: ['success']
  //           });
  //           // Swal.fire("Success", "Officer updated successfully!", "success");
  //           this.modalRef.close();
  //           this.statusForm.reset();
          
  //         } else {
  //         }
  //       },
  //       err => {
  //         this.isLoading = false;
  //       }
  //     );
  //   }
  // }



  logoutUser() { 
      this.commonService.logOut();
  }

  // END ----- Update user status block
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

}

