import { Injectable } from '@angular/core';
import { NetworkService } from '../shared/network.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private networkService: NetworkService) { }
  // validateLogin(req: any) {
  //   return this.networkService.login("api/admin/login", req, null, null);
  // }
  validateLogin(req: any) {
    return this.networkService.login("api/company/login", req, null, null);
  }
}
