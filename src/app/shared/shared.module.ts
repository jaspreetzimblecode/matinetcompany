
import { MenuItems } from './menu-items/menu-items';
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatDatepickerModule, MatInputModule, MatOptionModule, MatSelectModule, MatRadioModule, MatTableModule, MatButtonModule, MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule, MatMenuModule } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion'
import { MatTabsModule } from "@angular/material/tabs";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { MatNativeDateModule } from "@angular/material/core";
import { SpinnerComponent } from './spinner/spinner.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ResetbtnComponent } from '../main/resetbtn/resetbtn.component';
import { SearchbtnComponent } from '../main/searchbtn/searchbtn.component';
import { AddBtnComponent } from './add-btn/add-btn.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent,SearchbtnComponent,ResetbtnComponent, AddBtnComponent, EditComponent, DeleteComponent
  ],
  exports: [
    MatExpansionModule,
    FormsModule, ReactiveFormsModule,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatInputModule,
    MatOptionModule,
    MatIconModule,
    MatSelectModule,
    MatTabsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatRadioModule,
    MatMenuModule,
    MatSnackBarModule,
    MatTableModule,MatButtonModule,MatListModule,
    NgxDatatableModule,
    SpinnerComponent,SearchbtnComponent,ResetbtnComponent,AddBtnComponent,EditComponent, DeleteComponent
   ],
  providers: [ MenuItems ]
})
export class SharedModule { }
