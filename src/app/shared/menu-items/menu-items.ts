import { Injectable } from "@angular/core";

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  // { state:'dashboard', name: 'Dashboard', type: 'link', icon: 'av_timer' },
  { state: 'upload-vedio', type: 'link', name: 'Upload Video', icon: 'cloud_upload',iconUrl:'assets/images/vedioUpload.png' },
  { state: 'video', type: 'link', name: 'My Video', icon: 'cloud_upload',iconUrl:'assets/images/vedioUpload.png' },
 ];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
